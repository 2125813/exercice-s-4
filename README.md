## Exercice 1 [[voir exemple de solution dans le dépôt Cours](https://gitlab.com/CoursDali/420-4p3-hu-d-veloppement-web-en-asp.net/s3-les-vues-et-le-mod-le-de-donn-es/les-vues/cours)]
Dans une méthode d'action, créez une liste de chaînes de caractères de votre choix (exemple: liste de cours). Envoyez cette liste à une vue à l'aide de _ViewData_ ou _ViewBag_ et affichez les éléments de cette liste. 

## Exercice 2
Dans cet exercice:
- Vous devez créer ou réutiliser une application. 
- Vous devez décider quels contrôleurs, méthodes d'action et vues vous devez créer pour faire fonctionner la demande suivante.

L'application doit avoir 2 pages:
- Une page d'accueil.
- Une page dans laquelle on peut trouver plusieurs images. En dessous de chaque image, il doit y avoir au moins un titre (ajoutez ce que vous voulez comme informations). À la fin de la page, ajoutez un lien qui vous renvoi vers la page d'accueil.

**Partie recherche:**

Cherchez et trouvez comment ajouter une librairie côté client (css, javascript) à votre application et utilisez-la. Par exemple: ajoutez du style à votre page web de l'exercice 2.
